  <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="page-head-line">HASIL KONSULTASI</h4>
                </div>
            </div>
<?php
error_reporting(0);

function konversi_md($nilai){
    switch ($nilai) {
        case 0:
            return "Tidak";
            break;
        case 0.2:
            return "Kurang Yakin";
            break;
        case 0.4:
            return "Sedikit Yakin";
            break;
        case 0.6:
            return "Cukup Yakin";
            break;
        case 0.8:
            return "Yakin";
            break;
        case 1:
            return "Sangat Yakin";
            break;
    }
} 

if (!isset($_SESSION['GEJALA'])) {
  exit("<script>location.href='home.php?page=diagnosa';</script>");
}
$gejala = $_SESSION['GEJALA'];
$md_user = $_SESSION['MD_USER'];
$keterangan = array();
$cf = array();

# PROSES PERHITUNGAN CF

$q = mysqli_query($koneksi, "select * from keterangan");
if (mysqli_num_rows($q) > 0) {
  while ($h = mysqli_fetch_array($q)) {

    $id = $h['id_keterangan'];
    $keterangan[$id] = array($h['kode'], $h['nama']);

    $mb_lama = 0;
    $md_lama = 0;
    $mb_baru = 0;
    $md_baru = 0;
    $mb_sementara = 0;
    $md_sementara = 0;
    $gejala_ke = 0;

    $qq = mysqli_query($koneksi, "select * from nilaipakar where id_keterangan='" . $id . "' order by id_nilai");
    while ($hh = mysqli_fetch_array($qq)) {
        $id_gejala = $hh['id_gejala'];
      if (in_array($id_gejala, $gejala)) {
        $gejala_ke++;
        if ($gejala_ke == 1) {
          $mb_lama = 0;
          $md_lama = 0;
          $mb_baru = $hh['mb'];
          $md_baru = $md_user[$id_gejala];
          $mb_sementara = $hh['mb'];
          $md_sementara = $md_user[$id_gejala];
        } else {
          $mb_lama = $mb_sementara;
          $md_lama = $md_sementara;
          $mb_baru = $hh['mb'];
          $md_baru = $md_user[$id_gejala];
          $mb_sementara = $mb_lama + ($mb_baru * (1 - $mb_lama));
          $md_sementara = $md_lama + ($md_baru * (1 - $md_lama));
        }
      }
    }
    if ($gejala_ke > 0) {
      $nilai = round($mb_sementara - $md_sementara, 3);
      $nilai_keterangan[$id] = $nilai;
      $cf[] = array($nilai, $id);
    }
  }
}

sort($cf);

$nama_keterangan = '';
$daftar = '';
$no = 0;
for ($i = count($cf) - 1; $i >= 0; $i--) {
    if($cf[$i][0] > 0){
        if ($nama_keterangan == '') {
            $nama_keterangan = $keterangan[$cf[$i][1]][1];
            $pengendalian = $pengendalian[$cf[$i][1]][1];
            $nilai_tertinggi = $cf[$i][0] * 100;
        }
        $no++;
        $nilai = ($cf[$i][0] * 100);
        $kpp = $keterangan[$cf[$i][1]][0];
        $pp = $keterangan[$cf[$i][1]][1];
        $daftar .= '
            <tr>
            <td style="text-align:center;"><font color="#000000">' . $no . '</td>
            <td><font color="#000000">' . $keterangan[$cf[$i][1]][0] . '</td>
            <td><font color="#000000">' . $keterangan[$cf[$i][1]][1] . '</td>
            <td style="text-align:center;"><font color="#000000">' . $nilai . ' %</td>
            <td style="text-align:center;"><font color="#000000">' . $no . '</td>
            </tr>
        ';
    }
}

$list_gejala = '';
$no = 0;
$q = mysqli_query($koneksi, "select * from gejala order by kode");
if (mysqli_num_rows($q) > 0) {
  while ($h = mysqli_fetch_array($q)) {
    if (isset($_SESSION['GEJALA'])) {
        $md_user = $_SESSION['MD_USER'];
        $id_gejala = $h['id_gejala'];
      if (in_array($id_gejala, $_SESSION['GEJALA'])) {
        $no++;
        $list_gejala .= '
          <tr>
          <td valign="top" width="30">' . $no . '</td>
          <td valign="top" width="70">' . $h['kode'] . '</td>
          <td valign="top">' . $h['nama_gejala'] . ' ('.strtoupper(konversi_md($md_user[$id_gejala])).')</td>
          </tr>
        ';
      }
    }
  } 
}
$query = "INSERT INTO konsultasi
          (id_pasien,persentase,keterangan)
VALUES ('$_SESSION[iduser]','$nilai','$nama_keterangan')";
$result = mysqli_query($koneksi, $query)
or die(mysqli_error($koneksi));

$qw5 = mysqli_query($koneksi, "select * from keterangan where nama='$nama_keterangan'");
$hw5 = mysqli_fetch_array($qw5);
$aaa1 = $hw5['kode'];
?>
<div style="clear:both;height:20px;"></div>
Kondisi yang anda alami :
<table class="table table-striped table-hover table-bordered">
  <tbody>
    <?php echo $list_gejala; ?>
  </tbody>
</table>
</font>
<br/><br/>

<table class="table table-striped table-hover table-bordered">
  <thead>
    <tr>
      <th style="text-align:center;" width="30"><font color="#000000">NO</th>
      <th style="text-align:center;" width="100"><font color="#000000">KODE</th>
      <th style="text-align:center;"><font color="#000000">KETERANGAN</th>
      <th style="text-align:center;" width="70"><font color="#000000">CF</th>
    </tr>
  </thead>
  <tbody>
    <?php echo $daftar; ?>
  </tbody>
</table>
<table class="table table-bordered">
  <tbody>
    <tr>
    <td width="150" valign="top"><font color="#000000"><strong>SOLUSI</strong></font></td>
    <td><font color="#000000"><?php
$qw = mysqli_query($koneksi, "select rule.*,keterangan.* from rule, keterangan where keterangan.nama='$nama_keterangan' AND rule.kode_keterangan=keterangan.kode");
while ($hw = mysqli_fetch_array($qw)) {
  $no++;
  $solusi = $hw['kode_solusi'];
  $rr = mysqli_query($koneksi, "select * from solusi where kode='$solusi'");
  $dd = mysqli_fetch_array($rr);
  echo "$dd[solusi] <br>";
}
?></td>
    </tr>
    </tr>
  </tbody>
</table>
</div>
<center>
<a href="cetak_hasil.php?id=<?php echo $_SESSION['iduser']; ?>" target="_blank" class="btn btn-primary">Cetak</a>
</center>
</div></div>
 