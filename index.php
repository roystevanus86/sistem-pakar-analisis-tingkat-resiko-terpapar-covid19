<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    
    <title>Sistem Pakar Analisis Tingkat Resiko Terpapar Covid-19</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
     
</head>
<body>
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">            
                <a class="navbar-brand" href="index.php">
                    <h3><font color="#ffffff">SISTEM PAKAR ANALISIS<br/>TINGKAT RESIKO TERPAPAR COVID-19</font></h3>
                </a>
            </div>

            <div class="left-div">
                <div class="user-settings-wrapper">
                    <img src="assets/img/logo.png">
                </div>
            </div>
        </div>
    </div>
   
    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a href="index.php">BERANDA</a></li>
                            <li><a href="index.php?page=login">REGISTER/LOGIN</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- MENU SECTION END-->
    <?php
        if (isset($_GET['page'])){
            $page = $_GET['page'];
            if($page == 'login'){
                include('login.php');
            }
                        
        }else{
    ?>
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <center><h1 style="font-family:Vijaya; font-size: 35pt"><b>SELAMAT DATANG</h1>
                <h2 style="font-family:Vijaya;">Di Sistem Pakar Analisis Tingkat Resiko Terpapar Covid-19</h2>
                
            </div><br/>
            <div class="row">
                <div class="col-md-6 col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>INFORMASI</h4>
                        </div>
                        <div class="panel-body">
                            <p><b>Sistem pakar</b> adalah sistem berbasis komputer yang menggunakan pengetahuan, 
                            fakta dan teknik penalaran dalam memecahkan masalah yang biasanya hanya dapat 
                            dipecahkan oleh seorang pakar dalam bidang tersebut.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>PETUNJUK</h4>
                        </div>
                        <div class="panel-body">
                            <p>Cara Menggunakan Sistem Pakar ini: <br>
                            1. Untuk melakukan konsultasi dengan sistem Anda harus login. <br>
                            &nbsp;&nbsp;&nbsp;
                            *Belum memiliki akun dapat registrasi terlebih dahulu di menu <a href="?page=login"> Registrasi</a> <br>
                            2. Login. <br>
                            3. Pilih kondisi yang dialami, kemudian klik button analisis. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br>
    <?php
        }
    ?>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                Roy Stevanus Yapanto - 71150122 <br>
                2021
                </div>

            </div>
        </div>
    </footer>
    
    <script src="assets/js/jquery-1.11.1.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>