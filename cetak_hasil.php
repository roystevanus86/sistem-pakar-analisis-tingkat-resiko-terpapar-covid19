<body onload=javascript:window:print()>

<?php
error_reporting(0);
session_start();

include 'koneksi.php';

if (!isset($_SESSION['GEJALA'])) {
	exit("<script>location.href='home.php?page=diagnosa';</script>");
}
$gejala = $_SESSION['GEJALA'];
$md_user = $_SESSION['MD_USER'];
$nama_gejala = array();
for ($i = 0; $i < count($gejala); $i++) {
	$q = mysqli_query($koneksi, "select * from gejala where id_gejala='" . $gejala[$i] . "'");
	$h = mysqli_fetch_array($q);
	$nama_gejala[] = $h['nama_gejala'];-
}
$nama_gejala = implode(', ', $nama_gejala);
 
$keterangan = array();
$cf = array();

$q = mysqli_query($koneksi, "select * from keterangan");
if (mysqli_num_rows($q) > 0) {
  while ($h = mysqli_fetch_array($q)) {

    $id = $h['id_keterangan'];
    $keterangan[$id] = array($h['kode'], $h['nama']);

    $mb_lama = 0;
    $md_lama = 0;
    $mb_baru = 0;
    $md_baru = 0;
    $mb_sementara = 0;
    $md_sementara = 0;
    $gejala_ke = 0;

    $qq = mysqli_query($koneksi, "select * from nilaipakar where id_keterangan='" . $id . "' order by id_nilai");
    while ($hh = mysqli_fetch_array($qq)) {
        $id_gejala = $hh['id_gejala'];
      if (in_array($id_gejala, $gejala)) {
        $gejala_ke++;
        if ($gejala_ke == 1) {
          $mb_lama = 0;
          $md_lama = 0;
          $mb_baru = $hh['mb'];
          $md_baru = $md_user[$id_gejala];
          $mb_sementara = $hh['mb'];
          $md_sementara = $md_user[$id_gejala];
        } else {
          $mb_lama = $mb_sementara;
          $md_lama = $md_sementara;
          $mb_baru = $hh['mb'];
          $md_baru = $md_user[$id_gejala];
          $mb_sementara = $mb_lama + ($mb_baru * (1 - $mb_lama));
          $md_sementara = $md_lama + ($md_baru * (1 - $md_lama));
        }

      }
    }
    if ($gejala_ke > 0) {
      $nilai = round($mb_sementara - $md_sementara, 3);
      $nilai_keterangan[$id] = $nilai;
      $cf[] = array($nilai, $id);
    }
  }
}

sort($cf);

$nama_keterangan = '';
$daftar = '';
$no = 0;
for ($i = count($cf) - 1; $i >= 0; $i--) {
    if($cf[$i][0] > 0){
        if ($nama_keterangan == '') {
            $nama_keterangan = $keterangan[$cf[$i][1]][1];
            $pengendalian = $pengendalian[$cf[$i][1]][1];
        }
        $no++;
        $nilai = ($cf[$i][0] * 100);
        $kpp = $keterangan[$cf[$i][1]][0];
        $pp = $keterangan[$cf[$i][1]][1];
        $daftar .= '
            <tr>
            <td style="text-align:center;"><font color="#000000">' . $no . '</td>
            <td><font color="#000000">' . $keterangan[$cf[$i][1]][0] . '</td>
            <td><font color="#000000">' . $keterangan[$cf[$i][1]][1] . '</td>
            <td style="text-align:center;"><font color="#000000">' . $nilai . ' %</td>
            <td style="text-align:center;"><font color="#000000">' . $no . '</td>
            </tr>
        ';
    }
}

?>

<div align="center">
<div id="right_col" style="font-size:10px;width:770px " align="center">
	<table cellpadding="0" cellspacing="0" width="100%" >
	<tr>
	<td valign="top">
	<div style="text-align:center;font-family:Arial, Helvetica, sans-serif;line-height:26px;padding-top:0px;height:90px;">
	<span style="font-size:18px;font-weight:bold;"><br>Sistem Pakar Analisis Tingkat Resiko Terpapar Covid-19</span><br />
	</div>
	<div style="border-bottom:2px solid #000;"></div>
	<br>
	<div">
	<?php
$q = mysqli_query($koneksi, "SELECT * FROM pasien WHERE id_pasien='$_SESSION[iduser]'");
$h = mysqli_fetch_array($q);

$qw5 = mysqli_query($koneksi, "select * from keterangan where nama='$nama_keterangan'");
$hw5 = mysqli_fetch_array($qw5);
$aaa1 = $hw5['kode'];?>

<table width="100%" border="0" cellspacing="0" cellpadding="4" style="float:left;">
	<tr>
		<td valign="top">Nama Lengkap </td>
		<td>: <?php echo "$h[nama]"; ?></td>
	</tr>
	<tr>
		<td width="200" valign="top">Jenis Kelamin</td>
		<td>: <?php echo "$h[jk]"; ?></td>
	</tr>
	<tr>
		<td width="200" valign="top">Tanggal Konsultasi</td>
		<td>: <?php
		function tanggal_indonesia($tanggal){
        $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        );

        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    echo tanggal_indonesia(date('Y-m-d')); ?></td>
	</tr>
</table>
</div> <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<table width="100%" border="0" cellspacing="0" cellpadding="4" style="float:left;">
	<tr>
		<td valign="top">GEJALA</td>
		<td>: <?php echo $nama_gejala; ?></td>
	</tr>
	<tr>
		<td width="200" valign="top">KETERANGAN </td>
		<td>:<strong> <?php echo strtoupper($nama_keterangan); ?></strong></td>
	</tr>

	<tr>
		<td width="200" valign="top">SOLUSI</td>
		<td><?php
$qw = mysqli_query($koneksi, "select rule.*,keterangan.* from rule, keterangan where keterangan.nama='$nama_keterangan' AND rule.kode_keterangan=keterangan.kode");
while ($hw = mysqli_fetch_array($qw)) {
  $no++;
  $solusi = $hw['kode_solusi'];
  $rr = mysqli_query($koneksi, "select * from solusi where kode='$solusi'");
  $dd = mysqli_fetch_array($rr);
  echo ": $dd[solusi] <br>";
}
?></td>
	</tr>
</table>


	<div style="clear:both;height:20px"></div>
	<table width="100%" class="tabel_t4" cellpadding="0" cellspacing="0" border="1">
	<thead>
	<tr>
		<th align="center" width="30">NO</th>
		<th align="center" width="100">KODE</th>
		<th align="center">KETERANGAN</th>
		<th align="center" width="100">NILAI</th>
	</tr>
	</thead>
	<tbody>
		<?php
echo $daftar;
?>
	</tbody>
	</table>


	</td>
	</tr>
	</table>


