-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Feb 2022 pada 04.39
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `pass`, `nama`) VALUES
(1, 'admin', 'admin', 'administrator'),
(2, 'adminbaru', 'adminbaru', 'adminbaru');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `id_gejala` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama_gejala` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`id_gejala`, `kode`, `nama_gejala`) VALUES
(4, 'BP001', 'Kurang dari 7 jam waktu tidur'),
(5, 'BP002', 'Lebih dari 8 jam kerja'),
(6, 'BP003', 'Lebih dari 5 hari kerja'),
(7, 'BP004', 'Ruangan tertutup (memiliki sekat atau dinding)'),
(8, 'BP005', 'Terdapat jarak yang cukup dekat antar meja'),
(9, 'BP006', 'Menggunakan alat yang sering dipakai Bersama'),
(10, 'BP007', 'Memiliki penyejuk ruangan (AC atau kipas angin)'),
(11, 'BP008', 'Tidak terdapat sirkulasi udara'),
(12, 'BP009', 'Belum melakukan vaksinasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsultasi`
--

CREATE TABLE `konsultasi` (
  `id` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `persentase` float NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsultasi`
--

INSERT INTO `konsultasi` (`id`, `id_pasien`, `persentase`, `keterangan`) VALUES
(115, 1, -0, ''),
(116, 1, -0, ''),
(117, 8, 0, ''),
(118, 8, 0, ''),
(119, 8, 0, ''),
(120, 8, 10, 'Resiko Terpapar Besar'),
(121, 8, 4.8, 'Resiko Terpapar Besar'),
(122, 8, -0.016, ''),
(123, 8, -0.008, ''),
(124, 8, 4.8, 'Resiko Terpapar Besar'),
(125, 1, 0.7, 'Resiko Terpapar Menengah'),
(126, 1, 4.4, 'Resiko Terpapar Menengah'),
(127, 1, -0.004, ''),
(128, 1, 4.4, 'Resiko Terpapar Menengah'),
(129, 1, 1.8, 'Resiko Terpapar Menengah'),
(130, 1, 0.3, 'Resiko Terpapar Menengah'),
(131, 1, 0.3, 'Resiko Terpapar Menengah'),
(132, 1, 0.7, 'Resiko Terpapar Menengah'),
(133, 1, 1.8, 'Resiko Terpapar Menengah'),
(134, 1, -0.008, ''),
(135, 1, 4.4, 'Resiko Terpapar Menengah'),
(136, 1, 0, ''),
(137, 1, 0, ''),
(138, 1, 1.8, 'Resiko Terpapar Menengah'),
(139, 1, 0.3, 'Resiko Terpapar Menengah'),
(140, 1, 0.7, 'Resiko Terpapar Menengah'),
(141, 1, 0.3, 'Resiko Terpapar Menengah'),
(142, 1, 0.1, 'Resiko Terpapar Menengah'),
(143, 1, 0.2, 'Resiko Terpapar Menengah'),
(144, 1, 0, ''),
(145, 1, 0, ''),
(146, 1, 0.1, 'Resiko Terpapar Menengah'),
(147, 1, 0.1, 'Resiko Terpapar Menengah'),
(148, 1, 0.1, 'Resiko Terpapar Menengah'),
(149, 1, 0.1, 'Resiko Terpapar Menengah'),
(150, 1, 0.1, 'Resiko Terpapar Menengah'),
(151, 1, 1.3, 'Resiko Terpapar Menengah'),
(152, 1, 6, 'Resiko Terpapar Menengah'),
(153, 1, 26, 'Resiko Terpapar Menengah'),
(154, 1, 0.5, 'Resiko Terpapar Menengah'),
(155, 1, -0.006, ''),
(156, 1, 1.8, 'Resiko Terpapar Menengah'),
(157, 1, 3, 'Resiko Terpapar Menengah'),
(158, 1, 3, 'Resiko Terpapar Menengah'),
(159, 1, 7.6, 'Resiko Terpapar Menengah'),
(160, 1, 6, 'Resiko Terpapar Menengah'),
(161, 1, 54, 'Resiko Terpapar Menengah'),
(162, 1, 40, 'Resiko Terpapar Menengah'),
(163, 1, -0.06, ''),
(164, 1, 0, ''),
(165, 1, 5, 'Resiko Terpapar Menengah'),
(166, 1, 4.4, 'Resiko Terpapar Menengah'),
(167, 1, 1.1, 'Resiko Terpapar Menengah'),
(168, 1, 8, 'Resiko Terpapar Menengah'),
(169, 1, 0.2, 'Resiko Terpapar Menengah'),
(170, 1, 20, 'Resiko Terpapar Menengah'),
(171, 1, 0, ''),
(172, 1, -0.2, ''),
(173, 1, 60, 'Resiko Terpapar Menengah'),
(174, 1, 40, 'Resiko Terpapar Menengah'),
(175, 1, -0.04, ''),
(176, 1, 56, 'Resiko Terpapar Menengah'),
(177, 1, 8, 'Resiko Terpapar Menengah'),
(178, 1, 28, 'Resiko Terpapar Menengah'),
(179, 1, 5.1, 'Resiko Terpapar Menengah'),
(180, 1, 40, 'Resiko Terpapar Menengah'),
(181, 1, 20, 'Resiko Terpapar Menengah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaipakar`
--

CREATE TABLE `nilaipakar` (
  `id_nilai` int(11) NOT NULL,
  `id_keterangan` int(11) NOT NULL,
  `id_gejala` int(20) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `mb` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilaipakar`
--

INSERT INTO `nilaipakar` (`id_nilai`, `id_keterangan`, `id_gejala`, `kode`, `mb`) VALUES
(2, 3, 4, '', 0.8),
(3, 3, 5, '', 0.8),
(4, 3, 6, '', 0.6),
(5, 3, 7, '', 0.6),
(6, 3, 8, '', 0.6),
(7, 3, 9, '', 0.8),
(8, 3, 10, '', 0.6),
(9, 3, 11, '', 0.6),
(10, 3, 12, '', 0.8),
(11, 3, 4, '', 0.6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jk` varchar(15) NOT NULL,
  `pekerjaan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `username`, `pass`, `nama`, `jk`, `pekerjaan`) VALUES
(1, 'roystev24', '71150122roy', 'Roy Stevanus Yapanto', 'Laki-laki', 'Pegawai Swasta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rule`
--

CREATE TABLE `rule` (
  `id_rule` int(11) NOT NULL,
  `kode_keterangan` varchar(20) NOT NULL,
  `kode_solusi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rule`
--

INSERT INTO `rule` (`id_rule`, `kode_keterangan`, `kode_solusi`) VALUES
(5, 'KET03', 'S01'),
(6, 'KET03', 'S01'),
(7, 'KET03', 'S01'),
(8, 'KET03', 'S01'),
(9, 'KET03', 'S01'),
(10, 'KET03', 'S01'),
(11, 'KET03', 'S01'),
(12, 'KET03', 'S01'),
(13, 'KET03', 'S01'),
(14, 'KET03', 'S01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `solusi`
--

CREATE TABLE `solusi` (
  `id_solusi` int(11) NOT NULL,
  `kode_solusi` varchar(20) NOT NULL,
  `solusi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `solusi`
--

INSERT INTO `solusi` (`id_solusi`, `kode_solusi`, `solusi`) VALUES
(1, 'S01', 'Mengerjakan 5 M:\r\nMemakai masker,\r\nMenjaga jarak,\r\nMencuci tangan,\r\nMenghindari kerumunan,\r\nMengurangi mobilisasi.\r\n\r\nMengkonsumsi makanan yang sehat (sayur dan buah),\r\nKonsumsi vitamin,\r\nOlahraga,\r\nBerjemur matahari,\r\nKurangi stres\r\n\r\nHindari makanan fastfood (siap saji),\r\nTidak merokok,\r\nTidak minum minuman berakohol,\r\nHindari begadang\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tujuan`
--

CREATE TABLE `tujuan` (
  `id_tujuan` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tujuan`
--

INSERT INTO `tujuan` (`id_tujuan`, `kode`, `nama`) VALUES
(2, 'KET01', 'Resiko Terpapar Kecil'),
(3, 'KET02', 'Resiko Terpapar Menengah'),
(4, 'KET03', 'Resiko Terpapar Besar');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id_gejala`);

--
-- Indeks untuk tabel `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD PRIMARY KEY (`id`,`id_pasien`);

--
-- Indeks untuk tabel `nilaipakar`
--
ALTER TABLE `nilaipakar`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indeks untuk tabel `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id_rule`);

--
-- Indeks untuk tabel `solusi`
--
ALTER TABLE `solusi`
  ADD PRIMARY KEY (`id_solusi`);

--
-- Indeks untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  ADD PRIMARY KEY (`id_tujuan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id_gejala` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `konsultasi`
--
ALTER TABLE `konsultasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT untuk tabel `nilaipakar`
--
ALTER TABLE `nilaipakar`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `rule`
--
ALTER TABLE `rule`
  MODIFY `id_rule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `solusi`
--
ALTER TABLE `solusi`
  MODIFY `id_solusi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tujuan`
--
ALTER TABLE `tujuan`
  MODIFY `id_tujuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
